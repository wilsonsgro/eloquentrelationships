<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transformers\UserTransformer as Transformer;
use App\Repositories\UserRepositoryEloquent as Repository;

class UserController extends Controller
{
    /**
     * UserController constructor.
     * @param Request $request
     * @param Repository $repository
     */
    public function __construct(Request $request, Repository $repository)
    {
        $this->limit = $request->get('limit', 20);
        $this->repository = $repository;
    }
    /**
     * @return \Dingo\Api\Http\Response
     */
    public function collection()
    {
        if (is_numeric($this->limit)) {
            return $this->response->paginator($this->repository->paginate($this->limit), Transformer::class);
        }
        return $this->response->collection($this->repository->all(), Transformer::class);
    }

}