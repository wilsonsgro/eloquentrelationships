<?php

namespace App\Http\Controllers;

use App\Bear;
use App\Picnic;

class BearController extends Controller
{
    /**
     *
     */
    public function creating() {
        // create a bear
        $bearGrizzly = Bear::create([
            'name' => 'Grizzly',
            'type' => 'Black',
            'danger_level' => 10
        ]);

        $picnicYellowstone = Picnic::firstOrNew([
            'name' => 'Yellowstone',
            'taste_level' => 6
        ]);
        $picnicGrandCanyon = Picnic::firstOrNew([
            'name' => 'Grand Canyon',
            'taste_level' => 5
        ]);

        // link our bears to picnics ---------------------
        // for our purposes we'll just add all bears to both picnics for our many to many relationship
        $bearGrizzly->picnics()->attach($picnicYellowstone->id);
        $bearGrizzly->picnics()->attach($picnicGrandCanyon->id);

        dd('Create bear Grizzly');
    }

    public function getting() {
        // get all the bears
        $bears = Bear::all();

        dd($bears);

        // find a specific bear by id
        $bear = Bear::find(4);

        dd($bear);

        // find a bear by a specific attribute
        $bearLawly = Bear::where('name', '=', 'Lawly')->first();

        dd($bearLawly);

        // find a bear with danger level greater than 5
        $dangerousBears = Bear::where('danger_level', '>', 5)->get();

        dd($dangerousBears);
    }

    /**
     *
     */
    public function updating() {

        // let's change the danger level of Lawly to level 10

        // find the bear
        $lawly = Bear::where('name', '=', 'Lawly')->first();

        // change the attribute
        $lawly->danger_level = 10;

        // save to our database
        $lawly->save();

        dd($lawly);

    }

    /**
     *
     */
    public function deleting() {

        dd("function deleting");
        // find and delete a record
        $bear = Bear::find(1);
        $bear->delete();

        // delete a record
        Bear::destroy(1);

        // delete multiple records
        Bear::destroy(1, 2, 3);

        // find and delete all bears with a danger level over 5
        Bear::where('danger_level', '>', 5)->delete();

    }

    /**
     *
     */
    public function oneToOne() {

        // find a bear named Adobot
        $adobot = Bear::where('name', '=', 'Adobot')->first();

        // get the fish that Adobot has
        $fish = $adobot->fish;

        dd($fish);
        // get the weight of the fish Adobot is going to eat
        $fish->weight;

        // alternatively you could go straight to the weight attribute
        $adobot->fish->weight;

        dd("function oneToOne");

    }

    /**
     *
     */
    public function oneToMany() {

        // find the trees lawly climbs
        $lawly = Bear::where('name', '=', 'Lawly')->first();

        foreach ($lawly->trees as $tree)
            echo $tree->type . ' ' . $tree->age. '<br>';

        dd($tree->type . ' ' . $tree->age);


    }

    /**
     *
     */
    public function manyToMany() {
        // get the picnics that Cerms goes to ------------------------
        $cerms = Bear::where('name', '=', 'Cerms')->first();

        // get the picnics and their names and taste levels
        echo 'Get the picnics and their names and taste levels'. '<br>';
        foreach ($cerms->picnics as $picnic)
            echo $picnic->name . ' taste levels: ' . $picnic->taste_level. '<br>';

        // get the bears that go to the Grand Canyon picnic -------------
        $grandCanyon = Picnic::where('name', '=', 'Grand Canyon')->first();

        // show the bears
        echo '<br>';
        echo 'Show the bears'. '<br>';
        foreach ($grandCanyon->bears as $bear)
            echo $bear->name . ' ' . $bear->type . ' danger level: ' . $bear->danger_level. '<br>';


        dd("function manyToMany");
    }
}
