<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class User extends Model implements Transformable
{
    use TransformableTrait;

    const CREATED_AT = 'SYSTEM_CreatedAt';
    const UPDATED_AT = 'SYSTEM_UpdatedAt';
    const DELETED_AT = 'SYSTEM_DeletedAt';
    protected $table = 'users';
    protected $primaryKey  = 'ID';
    protected $fillable = [];

}
