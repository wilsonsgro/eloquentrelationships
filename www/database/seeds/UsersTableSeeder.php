<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Creting the first user to test authentication user by JSON Web Tokens
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'wilson',
            'email'=> 'wilson.sgro@libero.it',
            'password'=> Hash::make('123stella')
        ]);

    }
}
