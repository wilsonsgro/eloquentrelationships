<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
$api = app('Dingo\Api\Routing\Router');

/** Login Logout GetToke*/
$api->version('v1', function ($api) {
    $api->post('v1/login',  'App\Http\Controllers\Auth\LoginController@login');
    $api->post('v1/logout', 'App\Http\Controllers\Auth\LoginController@logout');
    $api->get( 'v1/token',   'App\Http\Controllers\Auth\LoginController@getToken');
});
/** Applications */
$api->version('v1', ['middleware' => [
    'api.auth'
]], function ($api) {
    /** Users */
    $api->get('v1/users','App\Http\Controllers\UserController@collection');
});

