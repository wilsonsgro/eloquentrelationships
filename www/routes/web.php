<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
use App\Bear;

Route::get('/', function () {
    return view('welcome');
});
/*
Route::get('/bear', function () {
    return view('bear');
});
*/
Route::get('bear/creating', 'BearController@creating');
Route::get('bear/getting', 'BearController@getting');
Route::get('bear/updating', 'BearController@updating');
Route::get('bear/deleting', 'BearController@deleting');
Route::get('bear/onetoone', 'BearController@oneToOne');
Route::get('bear/onetomany', 'BearController@oneToMany');
Route::get('bear/manytomany', 'BearController@manyToMany');

Route::get('bear', function() {
    return View::make('bear')
        // all the bears (will also return the fish, trees, and picnics that belong to them)
        ->with('bears',  Bear::with('trees', 'picnics') ->get());

});

Route::get('debug', function() {
    return View::make('debug');
});
