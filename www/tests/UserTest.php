<?php

class UserTest extends TestCase
{

    /**
     * @test
     *
     * Test: GET /api/authenticate.
     */
    public function it_authenticate_a_user()
    {

        $this->post('v1/login', ['email' => 'wilson.sgro@libero.it', 'password' => '123stella'])
            ->seeJsonStructure(['token']);
    }
}
