![N|Solid](http://www.hd-fractals.com/wp-content/uploads/previeweye.jpg)




*For the things we have to learn before we can do them, we learn by doing them*


# Eloquent Relationships



### Installation

```sh
$ docker-compose up -d
```
 * -d                         Detached mode: Run containers in the background,
                               print new container names.
```sh
$ docker ps -a
```
 * --all, -a	false	Show all containers (default shows just running)    
```sh
$ docker-compose exec eloquent bash 
or
$ sudo docker exec -i -t {CID} /bin/bash 
```
 * `api` is name of our service.
 
 * `{CID}`  can be the complete container id, truncated container id, or even the first few digits of the container ids amongst all the currently running containers. 
```sh
$ cd /var/www
$ cat .env.local > .env
$ composer update
$ php artisan config:clear
$ php artisan key:generate

``` 
* Environment Configuration: create file .env


### Configuration

        mariadb:
            image: mariadb
            ports:
                - "3306:3306"
            environment:
                - MYSQL_ROOT_PASSWORD=123stella
                - MYSQL_DATABASE=eloquent
                - MYSQL_USER=mysql
                - MYSQL_PASSWORD=123stella

 
* docker-compose.yml

### Start

[**http://eloquent.localhost/**](http://eloquent.localhost/)
 
 
### Tutorial Relationships
            
[**A Guide to Using Eloquent ORM in Laravel**](https://scotch.io/tutorials/a-guide-to-using-eloquent-orm-in-laravel#sample-application)            

Let’s create a sample application about bears. Let’s say there are many bears. There are different **types** of bears with different **weights** and **danger levels**.

We will also say there are fish. Each **fish belongs to a single bear** since bears don’t like sharing. This will be our **one-to-one relationship**.

Bears also love to climb trees. Each bear will have many trees they like to climb. This will be our **one-to-many relationship**.

There will also be picnics. Many bears can go to many picnics since they like to ransack as many delicious picnics as they can. This will be our many-to-many relationship.

Start tutorial
```sh
$ docker-compose exec eloquent bash
```


**migrate**


```sh
Docker|root@36d1130c22aa:/# php artisan make:migration bears
```
* Create class class Bears extends Migration
* After commands php artisan make:migration you will find file in this path: **/www/database/migrations**
* Copy content /filesmigrations/_2017_01_27_071926_bears into new file migration

        By default, these migrations will include an auto-incrementing id. 
        It will also include timestamps for the fields created_at and updated_at. 
        updated_at will be automatically updated whenever the record is updated

```sh
Docker|root@36d1130c22aa:/# php artisan make:migration fish
```
* Create class class Fish extends Migration
* Copy content /filesmigrations/_2017_01_27_072356_fish into new file migration

        Plural vs Singular Database Table Names With our bears, 
        we can create the standard plural table name (ie bears, picnics). 
        With fish, it’s different. Do we use fish or fishes? 
        The good news is we can use whichever we want and then override the defaults when defining our Eloquent model

```sh
Docker|root@36d1130c22aa:/# php artisan make:migration trees
```
* Create class class Trees extends Migration
* Copy content /filesmigrations/_2017_01_27_072739_trees into new file migration

```sh
Docker|root@36d1130c22aa:/# php artisan make:migration picnics
```
* Create class class Picnics extends Migration
* Copy content /filesmigrations/_2017_01_27_072911_picnics into new file migration

        Since we will be creating a one to many relationship, we will need two tables. 
        One for the picnics and another to link a bear to a picnic

```sh
Docker|root@36d1130c22aa:/# php artisan make:migration bearspicnics
```
* Create class class BearsPicnics extends Migration
* Copy content /filesmigrations/_2017_01_27_080813_bearspicnics into new file migration

        Now we will need a table to link our bears to a picnic. 
        We’ll create a pivot table here. 
        This is how we can define our many-to-many relationship.

* With our migration files made, migrate your database using artisan:
```sh
Docker|root@36d1130c22aa:/# php artisan migrate
```

Now that we have migrated our database, we will need to seed our database. The process of seeding however is inserting records into our database and this will require Eloquent! We will need to create our models before we can seed the database.

**Eloquent Models**

Let’s make our Eloquent models. This is also where we will define our relationships.
```sh
Docker|root@36d1130c22aa:/# php artisan make:model Bear
Docker|root@36d1130c22aa:/# php artisan make:model Fish
Docker|root@36d1130c22aa:/# php artisan make:model Tree
Docker|root@36d1130c22aa:/# php artisan make:model Picnic
```
* After commands php artisan make:model you will find file in this path: **/www/app**        
* Copy content _Bear into new file model, the same for: Fish, Tree,Picnic


**Bear model** 

```sh
Docker|root@36d1130c22aa:/# php artisan make:model Bear
```

        Mass Assignment We have to set our mass assignable attributes so that we make sure that only the attributes we want are allowed to be changed.
        Defining Relationships When defining relationships, the name of the function can be whatever you want it to be named. It makes sense here since we will be finding the fish that belongs to the bear. On the line return $this->hasOne('Fish') however, you will need to match the name of the Eloquent model that corresponds to that item.
        There are different ways we can define relationships. There are hasOne, hasMany, belongsTo, belongsToMany, and more. Read the Eloquent Relationship docs to see all the things you can do.
        Eloquent Model and Database Table Naming Conventions By default, when you define an Eloquent model, you name it for the singular term. In this case Bear. Eloquent will then look to the database for the lowercase and plural version of that word. In this case, this model will be linked to our bears table we created in our migration.
        
        
**Fish model**

```sh
Docker|root@36d1130c22aa:/# php artisan make:model Fish
```

        Like we talked about earlier, since we named our table fish, then it doesn’t follow convention. We will explicitly call out the database name using protected $table.
        Similar to how we defined our relationship in the Bear model, we will define the inverse of that relationship. A Fish belongs to a Bear.

**Picnic model**

```sh
Docker|root@36d1130c22aa:/# php artisan make:model Picnic
```
 
         Just like our other models, we have defined mass assignable attributes and relationships. When defining many-to-many relationships, you use belongsToMany() and not hasMany. hasMany is used for one-to-many relationships.
         Now that we have our migrations and models done, we can seed our database. We will use Eloquent for inserting into our database for our seeds.
         For more information on Eloquent concepts like creating Eloquent models, performing CRUD, or defining relationships, definitely read the Laravel Eloquent docs.
 
         
**seeding**  
       
See the file **/www/database/seeds/** BearAppSeeder.php  
     
        In our seeder file, we are creating bears, fish, picnics, and linking many bears to one picnic.
        
        Grabbing the ID of a Newly Created Record We need to grab the id of the inserted bears and picnic so we will save the record into a variable on creation. After doing this, we are able to pull the id using $bearLawly->id.
        
        Why do we do this? Why do we pull the id of a newly created record? There are a few reasons for this. One, so we can create our relationships correctly. Second, after seeding your database multiple times, the id of your records will always be incrementing since that’s how we set up our database. As you can see in the picture below, the id of our bears are 10, 11, and 12. Dynamically creating our relationships instead of hardcoding in the id lets us not worry about messing with our seed files after they have been created.
        
        With our seeder file ready to go, let’s go into the command line and execute our seeds.  
           
 
  ```sh
   Docker|root@36d1130c22aa:/# php artisan db:seed
  ```

**view**
 
```sh
Docker|root@36d1130c22aa:/# php artisan make:view bear
``` 

* After commands php artisan make:view you will find file in this path: **/www/resources/views/**  bear.blade.php        
* Copy content _bear.blade.php into new file view.

[**http://eloquent.localhost/bear**](http://eloquent.localhost/bear)


[Querying Relationships](https://scotch.io/tutorials/a-guide-to-using-eloquent-orm-in-laravel#querying-relationships)

**/routes/web.php** 
        
        Now this is where Eloquent gets fun. With most applications, you will have relationships amongst the parts of your database. We have already defined these: bears will have one fish and picnics will have many bears.
        
        Route::get('bear/creating', 'BearController@creating');
        
        Route::get('bear/getting', 'BearController@getting');
        
        Route::get('bear/updating', 'BearController@updating');
        
        Route::get('bear/deleting', 'BearController@deleting');
        
        Route::get('bear/onetoone', 'BearController@oneToOne');
 
Route::get('bear/creating', 'BearController@creating'):
  
[**http://eloquent.localhost/bear/creating**](http://eloquent.localhost/bear/creating)

    
###Laravel - Eloquent “Has”, “With”, “WhereHas” - What do they mean? 

**With**

`with()` is for **eager loading**. That basically means, along the main model, Laravel will preload the relationship(s) you specify. This is especially helpful if you have a collection of models and you want to load a relation for all of them. Because with eager loading you run only one additional DB query instead of one for every model in the collection.

Example:

`User > hasMany > Post`
    
    
    $users = User::with('posts')->get();
    foreach($users as $user){
        $users->posts; // posts is already loaded and no additional DB query is run
    }
    

**Has**

`has()` is to filter the selecting model based on a relationship. So it acts very similarly to a normal WHERE condition. If you just use `has('relation')` that means you only want to get the models that have at least one related model in this relation.

Example:

`User > hasMany > Post`
    
    
    $users = User::has('posts')->get();
    // only users that have at least one post are contained in the collection
    

**WhereHas**

`whereHas()` works basically the same as `has()` but allows you to specify additional filters for the related model to check.

Example:

`User > hasMany > Post`
    
    
    $users = User::whereHas('posts', function($q){
        $q->where('created_at', '>=', '2015-01-01 00:00:00');
    })->get();
    // only users that have posts from this year are returned

[Source](http://stackoverflow.com/questions/30231862/laravel-eloquent-has-with-wherehas-what-do-they-mean "Permalink to  Laravel - Eloquent “Has”, “With”, “WhereHas” - What do they mean? ") 
    
###Using Polymorphic Relations in Laravel

https://jordijoan.me/using-polymorphic-relations-laravel/


## Laravel Debugbar

This is a package to integrate [PHP Debug Bar](http://phpdebugbar.com/) with Laravel 5.
It includes a ServiceProvider to register the debugbar and attach it to the output. You can publish assets and configure it through Laravel.
It bootstraps some Collectors to work with Laravel and implements a couple custom DataCollectors, specific for Laravel.
It is configured to display Redirects and (jQuery) Ajax Requests. (Shown in a dropdown)
Read [the documentation](http://phpdebugbar.com/docs/) for more configuration options.

![Screenshot](https://cloud.githubusercontent.com/assets/973269/4270452/740c8c8c-3ccb-11e4-8d9a-5a9e64f19351.png)

Note: Use the DebugBar only in development. It can slow the application down (because it has to gather data). So when experiencing slowness, try disabling some of the collectors.

This package includes some custom collectors:
 - QueryCollector: Show all queries, including binding + timing
 - RouteCollector: Show information about the current Route.
 - ViewCollector: Show the currently loaded views. (Optionally: display the shared data)
 - EventsCollector: Show all events
 - LaravelCollector: Show the Laravel version and Environment. (disabled by default)
 - SymfonyRequestCollector: replaces the RequestCollector with more information about the request/response
 - LogsCollector: Show the latest log entries from the storage logs. (disabled by default)
 - FilesCollector: Show the files that are included/required by PHP. (disabled by default)
 - ConfigCollector: Display the values from the config files. (disabled by default)

Bootstraps the following collectors for Laravel:
 - LogCollector: Show all Log messages
 - SwiftMailCollector and SwiftLogCollector for Mail

And the default collectors:
 - PhpInfoCollector
 - MessagesCollector
 - TimeDataCollector (With Booting and Application timing)
 - MemoryCollector
 - ExceptionsCollector

It also provides a Facade interface for easy logging Messages, Exceptions and Time

### Issue

 `The only supported ciphers are AES-128-CBC and AES-256-CBC` 

![N|Solid](http://angelsforinnovation.dev.starteed.com/img/markdownimage/erroreloquent.png)

 
 resolve with these commands: 
```sh
$ php artisan config:clear
$ php artisan key:generate
```
        
### Test PHPUnit

```sh
$ php vendor/bin/phpunit 
```

### Useful Commands

```sh
$ docker-compose up -d nginx mysql
```
* command to run app

```sh
$ docker exec -it laradock_php-fpm_1   /bin/bash
or 
docker exec -it e4e46efdc4bb  /bin/bash
```
* command to connect container

```sh
$ docker restart c554c459c595ì
```
* command to restart container 

```sh
$ docker-compose down
```
* stop and remove all containers

```sh
$ docker stop $(docker ps -a -q)
```
* close all containers

```sh
$ docker rm $(docker ps -a -q)
```
* remove all containers

```sh
$ docker rmi $(docker images -q)
```
* remove all images

```sh
$ docker network ls
```
* list  brides of Docker

```sh
$ docker network inspect eloquent_default
```
* inspect network eloquent

```sh
$ export TERM=xterm
```
* to set TERM environment variable, for image Alpine Linux

```sh
$ docker exec -i eloquent_mariadb_1 mysql -uroot -psecret elquent < eloquent_test.sql
```
* import database

### Tech

* [Eloquent ORM in Laravel](https://scotch.io/tutorials/a-guide-to-using-eloquent-orm-in-laravel#sample-application) - A Guide to Using Eloquent ORM in Laravel
* [Migrate](https://laravel.com/docs/5.3/migrations) - Database: Migrations
* [Seed](https://laravel.com/docs/5.3/seeding) - Database: Seeding
* [Postman](https://chrome.google.com/webstore/detail/postman/fhbjgbiflinjbdggehcddcbncdddomop) - Postman - Chrome Web Store - Google
* [PHPUnit](https://phpunit.de/) - PHPUnit!
* [Eloquent Relationships](https://laravel.com/docs/5.3/eloquent-relationships) - Eloquent Relationships